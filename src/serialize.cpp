#include "serialize.h"
#include "email_entity.h"

#include <QFile>
#include <QByteArray>

namespace Reporter
{
namespace Email
{
namespace Serialize
{

// see https://stackoverflow.com/questions/42859279/curl-mail-attachment-imagines-exe-file-rar-zip-files
// and https://stackoverflow.com/questions/44728855/curl-send-html-email-with-embedded-image-and-attachment

namespace
{

constexpr const char* HEADER_FROM     = "From:";
constexpr const char* HEADER_TO       = "To:";
constexpr const char* HEADER_SUBJECT  = "Subject:";
constexpr const char* HEADER_REPLY_TO = "Reply-To:";
constexpr const char* HEADER_CC       = "Cc:";
constexpr const char* HEADER_END      = "MIME-Version: 1.0";

constexpr const char* BODY_START =
R"(Content-Type: multipart/mixed; boundary="MULTIPART-MIXED-BOUNDARY"

--MULTIPART-MIXED-BOUNDARY
)";

constexpr const char* BODY_TEXT_START =
R"(Content-Type: multipart/alternative; boundary="MULTIPART-ALTERNATIVE-BOUNDARY"

--MULTIPART-ALTERNATIVE-BOUNDARY
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: base64
Content-Disposition: inline
)";

constexpr const char* BODY_TEXT_END = "--MULTIPART-ALTERNATIVE-BOUNDARY--";

constexpr const char* BODY_ATTACHMENT_START       = "--MULTIPART-MIXED-BOUNDARY";
constexpr const char* BODY_ATTACHMENT_TYPE        = "Content-Type: ";
constexpr const char* BODY_ATTACHMENT_ENCODING    = "Content-Transfer-Encoding: ";
constexpr const char* BODY_ATTACHMENT_DISPOSITION = "Content-Disposition: attachment; filename=";

constexpr const char* BODY_END = "--MULTIPART-MIXED-BOUNDARY--";

constexpr const char* ENCODING_BASE64 = "base64";

const QMap<EmailEntity::AttachmentType, QString> contentTypes
{
    {EmailEntity::AttachmentType::Binary, "application/octet-stream"},
    {EmailEntity::AttachmentType::Unknown, "application/octet-stream"},
};

QString generateAddressLine(const QString& headline, const QString& emailAddress)
{
    if (emailAddress.isEmpty())
    {
        return  QString(headline).append("\n");
    }

    const auto name = emailAddress.left(emailAddress.indexOf('@'));

    return QString(headline).append(name + "<" + emailAddress + ">" + "\n");
}

void serializeHeader(const EmailEntity& email, QTextStream& stream)
{
    stream << generateAddressLine(HEADER_FROM,     email.from);
    stream << generateAddressLine(HEADER_TO,       email.to);
    stream << HEADER_SUBJECT << email.subject << "\n";
    stream << generateAddressLine(HEADER_REPLY_TO, email.replyTo);
    stream << generateAddressLine(HEADER_CC,       email.carbonCopy);
    stream << HEADER_END << "\n";
}

void serializeText(const EmailEntity& email, QTextStream& stream)
{
    if (email.body.isEmpty())
    {
        return;
    }

    stream << BODY_TEXT_START;
    stream << email.body << "\n";
    stream << BODY_TEXT_END << "\n";
}

void serializeAttachments(const EmailEntity& email, QTextStream& stream)
{
    QString contentType;

    for (const auto& attachment : email.attachments)
    {
        contentType = contentTypes.value(attachment.type);

        QFile file(attachment.filePath);

        if (!file.exists() || !file.open(QIODevice::ReadOnly))
        {
            return;
        }

        QByteArray fileEncodedToBase64 = file.readAll().toBase64();

        stream << BODY_ATTACHMENT_START       << "\n";
        stream << BODY_ATTACHMENT_TYPE        << " "  << contentType     << "\n";
        stream << BODY_ATTACHMENT_ENCODING    << " "  << ENCODING_BASE64 << "\n";
        stream << BODY_ATTACHMENT_DISPOSITION << "\"" << file.fileName().section("/",-1,-1) << "\"\n";
        stream << fileEncodedToBase64         << "\n";
    }
}

void serializeBody(const EmailEntity& email, QTextStream& stream)
{
    stream << BODY_START;

    serializeText(email, stream);
    serializeAttachments(email, stream);

    stream << BODY_END;
}

} // namespace

void serializeToStream(const EmailEntity& email, QTextStream& stream)
{
    serializeHeader(email, stream);
    serializeBody(email, stream);
}

} // namespace Serialize
} // namespace Email
} // namespace Reporter
