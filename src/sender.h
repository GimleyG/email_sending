#ifndef REPORTER_EMAIL_SENDER_H
#define REPORTER_EMAIL_SENDER_H

#include "email_entity.h"
#include "credentials.h"

#include <QSharedPointer>
#include <QString>
#include <QObject>

namespace Reporter
{
namespace Email
{

class Sender : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Sender)

public:

    using Ptr = QSharedPointer<Sender>;

public:

    Sender() = default;

    ~Sender() override = default;

public:

    /**
     * @brief init the sender configurations
     * \param [in] credentials - credentials to access to server
     */
    virtual void setCredentials(const Credentials& credentials) = 0;

    /**
     * @brief send an email
     * \param [in] email
     */
    virtual void send(const EmailEntity& email) = 0;

signals:

    /**
     * @brief emitted when email successfully sended.
     */
    void succesfullySended() const;

    /**
     * @brief emitted when mail not sent due error.
     */
    void sendFail(const QString& description) const;

};

} // namespace Email
} // namespace Reporter

#endif // REPORTER_EMAIL_SENDER_H
