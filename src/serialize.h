#ifndef REPORTER_EMAIL_SERIALIZE_H
#define REPORTER_EMAIL_SERIALIZE_H

#include "email_entity.h"

namespace Reporter
{
namespace Email
{
namespace Serialize
{

void serializeToStream(const EmailEntity& email, QTextStream& stream);

} // namespace Serialize
} // namespace Email
} // namespace Reporter

#endif // REPORTER_EMAIL_SENDER_H
