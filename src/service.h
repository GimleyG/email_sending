//#ifndef REPORTER_EMAIL_SERVICE_H
//#define REPORTER_EMAIL_SERVICE_H

//#include "reporter/delivery/service.h"
//#include "reporter/email/sender.h"
//#include "reporter/common/compressing.h"
//#include "reporter/crypto/decryptor.h"

//#include <QSharedPointer>
//#include <QString>

//namespace Reporter
//{
//namespace Email
//{

//class Service final : public Delivery::Service
//{
//    Q_OBJECT
//    Q_DISABLE_COPY(Service)

//public:

//    using Ptr = QSharedPointer<Service>;

//public:

//    // TODO: pass by const reference
//    Service(Sender::Ptr              sender,
//            Common::Compressing::Ptr compressor,
//            Crypto::Decryptor::Ptr   decryptor);

//public: // Delivery::Service

//    ~Service() override = default;

//    void init(const Settings& settings) override;

//    void configure(const Model::UserProvidedInformation& config) override;

//    void deliverFolder(const QString& path) override;

//    void deliverFile(const QString& path) override;

//private:

//    void handleCompressingSuccess();

//    void handleCompressingFail(const QString& description);

//    void handleSendFail(const QString& description);

//    void sendNextEmail();

//    void handleSendSuccess();

//private:

//    // TODO: make const pointers
//    Sender::Ptr              _sender;
//    Common::Compressing::Ptr _compressor;
//    Crypto::Decryptor::Ptr   _decryptor;

//    QList<QString> _archivesList;
//    int            _emailSequencyNumber;

//    EmailEntity _emailTemplate;
//    size_t      _maxEmailSize; // bytes
//    bool        _isInitilized = false;

//    QList<QMetaObject::Connection> _connections;

//};

//} // namespace Email
//} // namespace Reporter

//#endif // REPORTER_EMAIL_SERVICE_H
