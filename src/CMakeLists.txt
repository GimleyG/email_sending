cmake_minimum_required(VERSION 3.1)

set(CMAKE_VERBOSE_MAKEFILE ON)

if (NOT CMAKE_BUILD_TYPE)
    message(STATUS "No build type selected, default to release")
    set(CMAKE_BUILD_TYPE "Release")
endif()

set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -DDEBUG")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")

# Useful CMake options for Qt projects
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTORCC ON)

# Search desired Qt packages
set(QT_MINIMUM_VERSION 5.10.1)

find_package(Qt5Core ${QT_MINIMUM_VERSION} REQUIRED)
find_package(Qt5Widgets)
find_package(Qt5Xml)

# For finding Qt includes
include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${Qt5Widgets_INCLUDE_DIRS}
    ${Qt5Core_INCLUDE_DIRS}
    )

# openssl

set(OPENSSL_PREBUILD_DIR ${CMAKE_SOURCE_DIR}/3rdparty/openssl-prebuild/openssl-1.0.2r/windows/x64)

include_directories(${OPENSSL_PREBUILD_DIR}/release-include)

find_library(OPENSSL_LIB_PATH
             NAMES libeay32
             PATHS ${OPENSSL_PREBUILD_DIR}/release-lib)

# libcurl

set(LIBCURL_PREBUILD_DIR ${CMAKE_SOURCE_DIR}/3rdparty/libcurl/curl-7.65.0-win64-vc15)

include_directories(${LIBCURL_PREBUILD_DIR}/include)

find_library(LIBCURL_PATH
             NAMES libcurl
             PATHS ${LIBCURL_PREBUILD_DIR})

if (WIN32)
 set(OUT_DIRECTORY ${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/${CMAKE_BUILD_TYPE})
 set(OPENSSL_DLL_DIRECTORY ${OPENSSL_PREBUILD_DIR}/release)
 set(LIBCURL_DLL_DIRECTORY   ${LIBCURL_PREBUILD_DIR})

 # Copy libraries to both directories for developer builds and CI builds
 file(COPY ${OPENSSL_DLL_DIRECTORY}/libeay32.dll DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
 file(COPY ${OPENSSL_DLL_DIRECTORY}/ssleay32.dll DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})

 file(COPY ${OPENSSL_DLL_DIRECTORY}/libeay32.dll DESTINATION ${OUT_DIRECTORY})
 file(COPY ${OPENSSL_DLL_DIRECTORY}/ssleay32.dll DESTINATION ${OUT_DIRECTORY})

 file(COPY ${LIBCURL_DLL_DIRECTORY}/libcurl.dll DESTINATION ${CMAKE_RUNTIME_OUTPUT_DIRECTORY})
 file(COPY ${LIBCURL_DLL_DIRECTORY}/libcurl.dll DESTINATION ${OUT_DIRECTORY})

endif()

set(TEST_EMAIL_SENDING_EXE ${PROJECT_NAME})

set(TEST_EMAIL_SENDING_SRC
    main.cpp

    service.h

    sender.h
    sender_impl.h
    sender_impl.cpp

    parameters.h

    email_entity.h
    email_entity.cpp

    serialize.h
    serialize.cpp

    credentials.h

    curl_transport.h
    curl_transport.cpp
    )

add_executable(${TEST_EMAIL_SENDING_EXE} ${TEST_EMAIL_SENDING_SRC})
target_link_libraries(${TEST_EMAIL_SENDING_EXE}
    Qt5::Core
    ${OPENSSL_LIB_PATH}
    ${LIBCURL_PATH}
    )
