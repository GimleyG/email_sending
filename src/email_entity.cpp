#include "email_entity.h"
#include "serialize.h"

namespace Reporter
{
namespace Email
{

void EmailEntity::addBinaryAttachment(const QString& filePath)
{
    attachments.append( {filePath, AttachmentType::Binary} );
}

void EmailEntity::serializeToStream(QTextStream& stream) const
{
    Serialize::serializeToStream(*this, stream);
}

} // namespace Email
} // namespace Reporter
