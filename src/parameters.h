#ifndef REPORTER_EMAIL_PARAMETERS_H
#define REPORTER_EMAIL_PARAMETERS_H

#include <QString>
#include <QList>

namespace Reporter
{
namespace Email
{

/*!
 * \brief Structure to store the letter parameters
 */
struct Parameters
{
    QString from;
    QString to;
    QString subject;
    QString body;
};

} // namespace Email
} // namespace Reporter

#endif // REPORTER_EMAIL_PARAMETERS_H
