#ifndef REPORTER_CURL_TRANSPORT_H
#define REPORTER_CURL_TRANSPORT_H

#include <QObject>

#include "email_entity.h"
#include "credentials.h"

namespace Reporter
{

class CurlTransport : public QObject
{
    Q_OBJECT

public:

    void send(const Email::EmailEntity&    email,
              const Reporter::Credentials& credentials,
              QDataStream*           dataStream);

signals:

    void sendCompleted() const;

    void sendFailed(const QString& description) const;

};

} // namespace Reporter

#endif // REPORTER_CURL_TRANSPORT_H
