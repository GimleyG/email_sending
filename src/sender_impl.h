#ifndef REPORTER_EMAIL_SENDER_IMPL_H
#define REPORTER_EMAIL_SENDER_IMPL_H

#include "sender.h"
#include "curl_transport.h"

#include <QSharedPointer>
#include <QString>
#include <QProcess>
#include <QObject>

namespace Reporter
{
namespace Email
{

class SenderImpl final : public Sender
{
    Q_OBJECT
    Q_DISABLE_COPY(SenderImpl)

public:

    using Ptr = QSharedPointer<SenderImpl>;

public:

    SenderImpl();

    ~SenderImpl() override = default;

public:

    void setCredentials(const Credentials& credentials) override;

    void send(const EmailEntity& email) override;

    void send(const QList<EmailEntity>& emails);

private:

    void performSending();

private:

    CurlTransport _transport;
    Credentials   _credentials;

    QList<EmailEntity> _sendQueue;
};

} // namespace Email
} // namespace Reporter

#endif // REPORTER_EMAIL_SENDER_H
