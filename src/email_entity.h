#ifndef REPORTER_EMAIL_EMAIL_ENTITY_H
#define REPORTER_EMAIL_EMAIL_ENTITY_H

#include "parameters.h"

#include <QSharedPointer>
#include <QString>
#include <QObject>
#include <QTextStream>

namespace Reporter
{
namespace Email
{

struct EmailEntity
{

    enum class AttachmentType
    {
        Unknown = 0,
        Binary
    };

    struct Attachment
    {
        QString filePath;
        AttachmentType type;
    };

    QString from;
    QString to;
    QString replyTo;
    QString carbonCopy;
    QString subject;
    QString body;
    QList<Attachment> attachments;

    /**
     * @brief add binary attachment to attachments list
     * \param [in] filePath - path to attachment file
     */
    void addBinaryAttachment(const QString& filePath);

    /**
     * @brief serialize mail to datastream
     * \param [out] stream
     */
    void serializeToStream(QTextStream& stream) const;

};

} // namespace Email
} // namespace Reporter

#endif // REPORTER_EMAIL_EMAIL_ENTITY_H
