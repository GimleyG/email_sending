#include "reporter/email/service.h"

namespace Reporter
{
namespace Email
{

namespace
{

constexpr const char* TAG_LOGIN    = "login:";
constexpr const char* TAG_PASSWORD = "secret:";
constexpr const char* TAG_SERVER   = "server:";
constexpr const char* TAG_PORT     = "port:";

bool parseCredentialsString(const QString& string, Credentials& credentials)
{
    // the credentials string should consist:
    // login:{user@email}secret:{password}server:{server}port:{port}

    if (!string.contains(TAG_LOGIN)    ||
        !string.contains(TAG_PASSWORD) ||
        !string.contains(TAG_SERVER)   ||
        !string.contains(TAG_PORT) )
    {
        return false;
    }

    const auto loginPos    = string.indexOf(TAG_LOGIN) + QString(TAG_LOGIN).length();
    const auto loginLenth  = string.indexOf(TAG_PASSWORD) - loginPos;

    const auto passPos     = string.indexOf(TAG_PASSWORD) + QString(TAG_PASSWORD).length();
    const auto passLenth   = string.lastIndexOf(TAG_SERVER) - passPos;

    const auto serverPos   = string.lastIndexOf(TAG_SERVER) + QString(TAG_SERVER).length();
    const auto serverLenth = string.lastIndexOf(TAG_PORT) - serverPos;

    const auto portPos     = string.lastIndexOf(TAG_PORT) + QString(TAG_PORT).length();
    const auto portLenth   = string.length() - portPos;


    credentials.login         = string.mid(loginPos,  loginLenth);
    credentials.password      = string.mid(passPos,   passLenth);
    credentials.serverAddress = string.mid(serverPos, serverLenth);
    credentials.portNumber    = string.mid(portPos,   portLenth);

    if (credentials.login.isEmpty()         ||
        credentials.password.isEmpty()      ||
        credentials.serverAddress.isEmpty() ||
        credentials.portNumber.isEmpty()     )
    {
        return false;
    }

    return true;
}

inline void disconnectAndReleaseConnections(QList<QMetaObject::Connection>& connections)
{
    for (const auto& connection : connections)
    {
        QObject::disconnect(connection);
    }

    connections.clear();
}

} // namespace

Service::Service::Service(Sender::Ptr              sender,
                          Common::Compressing::Ptr compressor,
                          Crypto::Decryptor::Ptr   decryptor) :
    // TODO: maybe initialize members and base
    _sender(sender),
    _compressor(compressor),
    _decryptor(decryptor)
{

}

void Service::init(const Settings& settings)
{
    const auto encryptedCredentials = settings.credentials;
    QString credentialString;
    if (!_decryptor->decryptString(encryptedCredentials, credentialString))
    {
        emit initFail("Fail to encrypt credentials");
        return;
    }

    Credentials credentials;
    if (!parseCredentialsString(credentialString, credentials))
    {
        emit initFail("Fail to parse credentials");
    }

    _sender->setCredentials(credentials);

    if (settings.receiverAddress.isEmpty())
    {
        emit initFail("Recepient address empty");
    }

    _emailTemplate = {};
    _emailTemplate.to = settings.receiverAddress;
    _emailTemplate.from = credentials.login;

    _maxEmailSize = settings.maxArchiveSize;

    _isInitilized = true;
    emit initialized();
}

void Service::configure(const Model::UserProvidedInformation& config)
{
    _emailTemplate.subject = "User: " + config.userEmail + " "
                             "Problem: " + config.problemCategory;

    _emailTemplate.body =
            "Category :" + config.problemCategory + " - " + config.problemSubcategory + " \\n"
            "User email : " + config.userEmail + " \\n"
            "Application name : " + config.applicationName + "\\n"
            "Description : " + config.description;
}

void Service::deliverFolder(const QString& path)
{
    if (!_isInitilized)
    {
        emit deliveryFail("Service not initilized");
    }

    disconnectAndReleaseConnections(_connections);

    _connections = {
        QObject::connect(_compressor.data(), &Common::Compressing::compressFail,
                         this, &Service::handleCompressingFail),
        QObject::connect(_compressor.data(), &Common::Compressing::compressed,
                         this, &Service::handleCompressingSuccess)
    };

    _compressor->compressDirectoryAndSplit(path, _maxEmailSize, _archivesList);
}

void Service::deliverFile(const QString& path)
{
    Q_UNUSED(path);

    emit deliveryFail("deliverFile method not implemented");
}

void Service::handleCompressingSuccess()
{
    disconnectAndReleaseConnections(_connections);

    _connections.append(
                QObject::connect(_sender.data(), &Sender::sendFail,
                                 this, &Service::handleSendFail));

    if (_archivesList.isEmpty())
    {
        emit deliveryFail("Internal error : the archive list is empty!");
    }

    if (_archivesList.size() == 1)
    {
        _connections.append(
                    QObject::connect(_sender.data(), &Sender::succesfullySended,
                                     this, &Service::handleSendSuccess));

        _emailTemplate.addBinaryAttachment(_archivesList.at(0));
        _sender->send(_emailTemplate);
        return;
    }

    _connections.append(
                QObject::connect(_sender.data(), &Sender::succesfullySended,
                                 this, &Service::sendNextEmail));

    _emailSequencyNumber = 0;

    sendNextEmail();
}

void Service::handleCompressingFail(const QString& description)
{
    disconnectAndReleaseConnections(_connections);

    emit deliveryFail("Compressing fail : " + description);
}

void Service::handleSendFail(const QString& description)
{
    disconnectAndReleaseConnections(_connections);

    emit deliveryFail("Sending fail : " + description);
}

void Service::sendNextEmail()
{
    if (_emailSequencyNumber >= _archivesList.size())
    {
        handleSendSuccess();
    }

    if (_emailSequencyNumber < 0)
    {
        disconnectAndReleaseConnections(_connections);

        emit deliveryFail("Internal error : sequency number less zero");
    }

    ++_emailSequencyNumber;

    auto email = _emailTemplate;

    email.subject.append(" #" + QString(_emailSequencyNumber));
    email.addBinaryAttachment(_archivesList.at(_emailSequencyNumber - 1));

    _sender->send(email);
}

void Service::handleSendSuccess()
{
    disconnectAndReleaseConnections(_connections);

    emit succesfullyDelivered();
}

} // namespace Email
} // namespace Reporter
