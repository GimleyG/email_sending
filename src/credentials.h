#ifndef REPORTER_COMMON_CREDENTIALS_H
#define REPORTER_COMMON_CREDENTIALS_H

#include <QString>

namespace Reporter
{
/** @brief Structure to store the credentials of the sender.
 */
struct Credentials
{
    QString login;
    QString password;
    QString server;     ///< server address, e.g. gmail.com.
    QString portNumber;
};

} // namespace Reporter

#endif // REPORTER_COMMON_CREDENTIALS_H
