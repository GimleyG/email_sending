#include <QCoreApplication>

#include <QDirIterator>
#include <QStandardPaths>

#include "email_entity.h"
#include "credentials.h"
#include "sender_impl.h"

namespace
{

QList<Reporter::Email::EmailEntity> prepareEmails()
{
    QList<Reporter::Email::EmailEntity> emails;

    Reporter::Email::EmailEntity entity;
    entity.from    = entity.replyTo =QStringLiteral("");
    entity.to      = QStringLiteral("");
    entity.body    = QStringLiteral("I cannot play. I want to play");

    const QString commonSubject = QStringLiteral("\"Clash of clans\": Google problem");

    QDirIterator directoryIterator("", QDir::Files);

    int part = 0;
    while (directoryIterator.hasNext())
    {
        entity.subject = commonSubject + QStringLiteral(" part#") + QString::number(part);

        QFileInfo fileInfo(directoryIterator.next());
        entity.attachments = { { fileInfo.absoluteFilePath(), Reporter::Email::EmailEntity::AttachmentType::Binary} };

        emails.append(entity);
        part++;
    }

    return emails;
}

Reporter::Credentials prepareCredentials()
{
    Reporter::Credentials credentials;

    credentials.login      = QStringLiteral("");
    credentials.password   = QStringLiteral("");
    credentials.server     = QStringLiteral("");
    credentials.portNumber = QStringLiteral("");

    return credentials;
}

} // namespace

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QList<Reporter::Email::EmailEntity> emails      = prepareEmails();
    Reporter::Credentials               credentials = prepareCredentials();

    Reporter::Email::SenderImpl::Ptr sender = Reporter::Email::SenderImpl::Ptr::create();
    sender->setCredentials(credentials);

    QObject::connect(sender.data(), &Reporter::Email::SenderImpl::succesfullySended,
                     &a,            &QCoreApplication::quit);

    QObject::connect(sender.data(), &Reporter::Email::SenderImpl::sendFail,
                     &a,            &QCoreApplication::quit);

    sender->send(emails);

    return a.exec();
}
