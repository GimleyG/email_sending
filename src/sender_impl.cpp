#include "sender_impl.h"

#include "email_entity.h"

#include <QTemporaryFile>
#include <QTextStream>

namespace Reporter
{
namespace Email
{

SenderImpl::SenderImpl() :
    Sender()
{
    QObject::connect(&_transport, &CurlTransport::sendCompleted,
                     this,        &SenderImpl::performSending);

    QObject::connect(&_transport, &CurlTransport::sendFailed,
                     this,        &SenderImpl::sendFail);
}

void SenderImpl::setCredentials(const Credentials& credentials)
{
    _credentials = credentials;
}

void SenderImpl::send(const EmailEntity& email)
{
    _sendQueue.clear();
    _sendQueue.append(email);

    performSending();
}

void SenderImpl::send(const QList<EmailEntity>& emails)
{
    _sendQueue = emails;

    performSending();
}

void SenderImpl::performSending()
{
    if (_sendQueue.empty())
    {
        emit succesfullySended();
        return;
    }

    auto email = _sendQueue.front();
    _sendQueue.removeFirst();

    qInfo(qPrintable("Sending " + email.subject));

    QTemporaryFile mailFile;
    mailFile.setAutoRemove(false);
    if (!mailFile.open())
    {
        // TODO: abort operation if this failed (return?)
        emit sendFail("Fail to create temprorary file.");
        return;
    }

    QTextStream fileStream(&mailFile);
    email.serializeToStream(fileStream);
    mailFile.reset();

    QDataStream dataStream(&mailFile);

    _transport.send(email, _credentials, &dataStream);

    mailFile.close();
}

} // namespace Email
} // namespace Reporter
