#include "curl_transport.h"

#include <stdio.h>
#include <io.h>
#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>

namespace Reporter
{

namespace
{

struct upload_status
{
    QDataStream* dataStream;
};

size_t payload_source(void* buffer, size_t size, size_t nitems, void* userdata)
{
    if((size == 0) || (nitems == 0) || ((size * nitems) < 1))
    {
        return 0;
    }

    upload_status* upload_ctx = reinterpret_cast<upload_status*>(userdata);

    size_t bytesRead = upload_ctx->dataStream->readRawData(reinterpret_cast<char*>(buffer), size * nitems);

    return (bytesRead == -1) ? 0 : bytesRead;
}

} // namespace

void CurlTransport::send(const Email::EmailEntity& email,
                         const Reporter::Credentials& credentials,
                         QDataStream* dataStream)
{
    CURL* curl = curl_easy_init();

    if (!curl)
    {
        emit sendFailed(QStringLiteral("Failed to initialize curl connection."));

        return;
    }

    CURLcode res = CURLE_OK;

    curl_slist* recipients = NULL;
    upload_status upload_ctx { dataStream };

    curl_easy_setopt(curl, CURLOPT_URL,      qPrintable(credentials.server));
    curl_easy_setopt(curl, CURLOPT_USERNAME, qPrintable(credentials.login));
    curl_easy_setopt(curl, CURLOPT_PASSWORD, qPrintable(credentials.password));

    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);

    curl_easy_setopt(curl, CURLOPT_MAIL_FROM,  qPrintable(email.from));
    recipients = curl_slist_append(recipients, qPrintable(email.to));
    curl_easy_setopt(curl, CURLOPT_MAIL_RCPT, recipients);

    /* We're using a callback function to specify the payload (the headers and
     * body of the message). You could just use the CURLOPT_READDATA option to
     * specify a FILE pointer to read from. */

    curl_easy_setopt(curl, CURLOPT_READFUNCTION, payload_source);
    curl_easy_setopt(curl, CURLOPT_READDATA, &upload_ctx);
    curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);

    curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

    res = curl_easy_perform(curl);

    if (res != CURLE_OK)
    {
        // TODO: fix this code duplication
        curl_slist_free_all(recipients);
        curl_easy_cleanup(curl);

        QString reason = QString("Failed to send: %1").arg(curl_easy_strerror(res));
        emit sendFailed(reason);

        return;
    }

    curl_slist_free_all(recipients);

    // Note: this will close the connection, if several mails should be sent -
    // do not close it until all is sent
    curl_easy_cleanup(curl);

    emit sendCompleted();
}

} // namespace Reporter
